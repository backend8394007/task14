const fs = require('fs');
let path = '';
let text = '';
const readlineSync = require('readline-sync');
//let mode = readlineSync.question('\n1. Read the file \n2. Write the file\n');
let fileDescriptor;

function setPath() {
    path = readlineSync.question('Enter the path to the file: ');
}

function setText() {
    text = readlineSync.question('Enter the text: ');
}

function createOrOpenFile() {
    try {
        fileDescriptor = fs.openSync(path, 'wx+');
        console.log('Файл создан');
    } catch (error) {
        if (error.code === 'EEXIST') {
            console.log('Файл уже существует');
            fileDescriptor = fs.openSync(path, 'r+');
            console.log('Файл открыт');
        } else {
            throw error;
        }
    }
}

function readTextFromFile() {
    try {
        const data = fs.readFileSync(path);
        console.log('Text: ' + data.toString());
    } catch (error) {
        throw error;
    }
}

function writeTextToFile() {
    try {
        fs.writeFileSync(path, text);
    } catch (error) {
        throw error;
    }
}

function closeFile() {
    try {
        fs.closeSync(fileDescriptor);
        console.log('Файл закрыт!');
    } catch (error) {
        throw error;
    }
}

setPath();
createOrOpenFile();
setText();
writeTextToFile();
readTextFromFile();
closeFile();
/*
switch (parseInt(mode.toString())) {
    case 1:
        setPath();
        createOrOpenFile();
        readTextFromFile();
        closeFile();
        break;
    case 2:
        setPath();
        createOrOpenFile();
        setText();
        writeTextToFile();
        closeFile();
        break;
    default:
        console.log('Такого режима нет!');
        break;
}
*/